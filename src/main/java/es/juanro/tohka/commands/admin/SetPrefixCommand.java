package es.juanro.tohka.commands.admin;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class SetPrefixCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        Member member = event.getMember();
        TextChannel channel = event.getChannel();

        if (!member.hasPermission(Permission.MANAGE_SERVER)) {
            channel.sendMessage("You need the Manage Server permission to use this command!").queue();
            return;
        }

        if (args.isEmpty()) {
            channel.sendMessage("Not enough arguments! Please check`" + Constants.PREFIX + " help " + getInvoke() +
                    "` for more details.").queue();
            return;
        }

        String newPrefix = args.get(0);

        Constants.PREFIXES.put(event.getGuild().getIdLong(), newPrefix);
        channel.sendMessage("The prefix has been changed to `" + newPrefix + "` sucessfully! \n"
                + "Also, remember that you can always also use `!t` as a prefix!").queue();
    }

    @Override
    public String getHelp() {
        return "Sets the prefix for the bot. \n" +
                "Usage: " + Constants.PREFIX + getInvoke() + "<Prefix>";
    }

    @Override
    public String getInvoke() {
        return "setprefix";
    }
}
