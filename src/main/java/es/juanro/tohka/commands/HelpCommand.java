package es.juanro.tohka.commands;

import es.juanro.tohka.CommandManager;
import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.util.List;

public class HelpCommand implements ICommand {

    private final CommandManager manager;

    public HelpCommand(CommandManager manager) {
        this.manager = manager;
    }

    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        if (args.isEmpty()) {
            EmbedBuilder help = new EmbedBuilder();
            help.setAuthor("Tohka", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png");
            help.setColor(new Color(176, 97, 255));
           /* StringBuilder commandList = new StringBuilder();
            manager.getCommands().forEach(
                    (command) -> commandList.append('`').append(command.getInvoke()).append("`\n"));
            help.setDescription(commandList); */
            help.addField("TOOLS", "`avatar help ping serverinfo setprefix user`", false);
            help.addField("MODERATION", "`ban kick purge unban`", false);
            help.addField("MUSIC", "`join leave nowplaying pause play queue resume skip stop`", false);
            help.addField("CONVERT", "`c2f f2c cm2ft ft2cm m2k k2m`", false);
            help.addField("FUN", "`lowres uwu`", false);
            help.addField("", "Type !t help <CommandName> for an explanation of each command! \n" +
                    "Also, remember that you can always use `!t` as a prefix!", false);
            event.getChannel().sendMessage(help.build()).queue();
            return;
        }

        ICommand command = manager.getCommand(String.join("", args));
        if (command == null) {
            event.getChannel().sendMessage("That command does not exist! Type " + Constants.PREFIX + "help for a list of commands!").queue();
            return;
        }
        EmbedBuilder help = new EmbedBuilder();
        help.setAuthor("Tohka", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png");
        help.setColor(new Color(176, 97, 255));
        help.addField(command.getInvoke(), command.getHelp(), false);
        event.getChannel().sendMessage(help.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Displays all the commands of the bot. Using " + Constants.PREFIX + getInvoke() + "<CommandName> shows you a detailed explanation for each command.";
    }

    @Override
    public String getInvoke() {
        return "help";
    }
}
