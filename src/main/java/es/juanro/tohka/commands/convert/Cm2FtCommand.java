package es.juanro.tohka.commands.convert;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class Cm2FtCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        double cm = Double.parseDouble(args.get(0));
        double feet = cm/30.48;
        event.getChannel().sendMessage(String.format("%.2f cm are %.2f ft", cm, feet)).queue();
    }

    @Override
    public String getHelp() {
        return "Converts a value in centimeters to feet. You can enter decimals by using \".\"! \n"
                + "Usage: " + getInvoke() + "<Amount in centimeters>";
    }

    @Override
    public String getInvoke() {
        return "cm2ft";
    }
}
