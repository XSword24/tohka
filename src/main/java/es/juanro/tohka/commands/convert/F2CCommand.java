package es.juanro.tohka.commands.convert;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class F2CCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        int fahrenheit = Integer.parseInt(args.get(0));
        int celsius = (fahrenheit - 32) * 5 / 9;
        event.getChannel().sendMessage(String.format("%s \u00BAC is %s \u00BAF.", fahrenheit, celsius)).queue();
    }

    @Override
    public String getHelp() {
        return "Transforms a value in Fahrenheit to Celsius. \n"
                + "Usage: " + Constants.PREFIX + getInvoke() + " <Amount in Fahrenheit>";
    }

    @Override
    public String getInvoke() {
        return "f2c";
    }
}
