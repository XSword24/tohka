package es.juanro.tohka.commands.convert;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class M2KCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        double miles = Double.parseDouble(args.get(0));
        double kilometers = miles * 1.609;
        event.getChannel().sendMessage(String.format("%.2f mi is %.2f km.", miles, kilometers)).queue();
    }

    @Override
    public String getHelp() {
        return "Transforms a value in Miles to Kilometers. \n"
                + "Usage: " + Constants.PREFIX + getInvoke() + " <Amount in Miles>";
    }

    @Override
    public String getInvoke() {
        return "m2k";
    }
}
