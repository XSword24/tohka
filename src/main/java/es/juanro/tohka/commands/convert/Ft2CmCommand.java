package es.juanro.tohka.commands.convert;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class Ft2CmCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        double feet = Double.parseDouble(args.get(0));
        double cm = feet*30.48;
        event.getChannel().sendMessage(String.format("%.2f ft are %.2f cm", feet, cm)).queue();
    }

    @Override
    public String getHelp() {
        return "Converts a value in feet to centimeters. You can enter decimals by using \".\"! \n"
                + "Usage: " + getInvoke() + "<Amount in feet>";
    }

    @Override
    public String getInvoke() {
        return "ft2cm";
    }
}
