package es.juanro.tohka.commands.convert;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class K2MCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        double kilometers = Double.parseDouble(args.get(0));
        double miles = 0.6214*kilometers;
        event.getChannel().sendMessage(String.format("%.2f km is %.2f mi.", kilometers, miles)).queue();
    }

    @Override
    public String getHelp() {
        return "Transforms a value in Kilometers to Miles. \n"
                + "Usage: " + Constants.PREFIX + getInvoke() + " <Amount in Kilometers>";
    }

    @Override
    public String getInvoke() {
        return "k2m";
    }
}
