package es.juanro.tohka.commands.convert;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class C2FCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        int celsius = Integer.parseInt(args.get(0));
        int fahrenheit = (celsius * 9 / 5) + 32;
        event.getChannel().sendMessage(String.format("%s \u00BAC is %s \u00BAF.", celsius, fahrenheit)).queue();
    }

    @Override
    public String getHelp() {
        return "Transforms a value in Celsius to Fahrenheit. \n"
                + "Usage: " + Constants.PREFIX + getInvoke() + " <Amount in Celsius>";
    }

    @Override
    public String getInvoke() {
        return "c2f";
    }
}
