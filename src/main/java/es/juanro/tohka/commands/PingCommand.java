package es.juanro.tohka.commands;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class PingCommand implements ICommand {

    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        event.getChannel().sendMessage("Ping is " + event.getJDA().getGatewayPing() + " ms").queue();
    }

    @Override
    public String getHelp() {
        return "Pong!";
    }

    @Override
    public String getInvoke() {
        return "ping";
    }
}
