package es.juanro.tohka.commands;

import com.jagrosh.jdautilities.commons.utils.FinderUtil;
import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfoCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        if (args.isEmpty()) {
            event.getChannel().sendMessage("Not enough arguments! Check " + Constants.PREFIX + getInvoke() + " for more details.").queue();
            return;
        }
        String joined = String.join("", args);
        List<User> foundUsers = FinderUtil.findUsers(joined, event.getJDA());

        if (foundUsers.isEmpty()) {
            List<Member> foundMembers = FinderUtil.findMembers(joined, event.getGuild());
            if (foundMembers.isEmpty()) {
                event.getChannel().sendMessage("No users found for `" + joined + "`").queue();
                return;
            }

            foundUsers = foundMembers.stream().map(Member::getUser).collect(Collectors.toList());
        }
        User user = foundUsers.get(0);
        Member member = event.getGuild().getMember(user);

        MessageEmbed embed = new EmbedBuilder()
                .setColor(new Color(176, 97, 255))
                .setThumbnail(user.getEffectiveAvatarUrl())
                .addField("Username #Discriminator", String.format("%#s", user), false)
                .addField("Display name", member.getEffectiveName(), false)
                .addField("Account creation date", member.getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME), false)
                .addField("Join date",member.getTimeJoined().format(DateTimeFormatter.RFC_1123_DATE_TIME), false)
                .addField("Boosting the server since", checkBoost(member), false)
                .build();
        event.getChannel().sendMessage(embed).queue();
    }

    @Override
    public String getHelp() {
        return "Returns information about a user.";
    }

    @Override
    public String getInvoke() {
        return "user";
    }

    public String checkBoost(Member member) {

       if (member.getTimeBoosted() == null){
           return "This user is not boosting the server! Shame on him!";
       }
       return member.getTimeBoosted().format(DateTimeFormatter.RFC_1123_DATE_TIME);
    }

}
