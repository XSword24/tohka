package es.juanro.tohka.commands;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class UwuCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        if (event.getMessage().getContentRaw().length() == 6) {
            event.getChannel().sendMessage("uwu").queue();
        }
        String original = event.getMessage().getContentRaw();
        String[] words = original.split(" ");
        String uwu = "";
        for (int h = 2; h < words.length; h++) {
            String temp = words[h];
            for (int i = 0; i < words[h].length(); i++) {
                if (i != 0 && i % 2 == 0) {
                    uwu = uwu + "uwu" + temp.charAt(i);
                } else {
                    uwu = uwu + temp.charAt(i);
                }
            }
            if (temp.length() == 2) {
                uwu = uwu + "uwu";
            }
            uwu = uwu + " ";
        }
        event.getChannel().sendMessage(uwu).queue();
    }

    @Override
    public String getHelp() {
        return "Translates text to uwuspeak. \n" +
                "Usage: "+ Constants.PREFIX + getInvoke() + " <Text>";
    }

    @Override
    public String getInvoke() {
        return "uwu";
    }
}
