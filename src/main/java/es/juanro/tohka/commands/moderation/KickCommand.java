package es.juanro.tohka.commands.moderation;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class KickCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        Member member = event.getMember();
        Member selfMember = event.getGuild().getSelfMember();
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();

        if (args.isEmpty() || mentionedMembers.isEmpty()) {
            event.getChannel().sendMessage("Not enough arguments! Check " + Constants.PREFIX + getInvoke() +
                    " for more details.").queue();
            return;
        }

        Member target = mentionedMembers.get(0);
        String reason = String.join(" ", args.subList(1, args.size()));

        if (target == selfMember) {
            event.getChannel().sendMessage("You shouldn't do that!").queue();
        }

        if (!member.hasPermission(Permission.KICK_MEMBERS)) {
            event.getChannel().sendMessage("You need the adequate permissions to use this command!").queue();
            return;
        }
        if (!event.getGuild().getSelfMember().canInteract(target)) {
            event.getChannel().sendMessage("You can't ban this user!").queue();
            return;
        }

        if (!selfMember.hasPermission(Permission.KICK_MEMBERS) && !selfMember.canInteract(target)) {
            event.getChannel().sendMessage("I can't kick that user or I don't have the kick permission!").queue();
            return;
        }
        event.getGuild().kick(target, String.format("Kicked by %#s, with reason %s", event.getAuthor(),
                reason)).queue();
        event.getChannel().sendMessage("Kicked user " + target.getUser().getAsMention() + " for reason: " + reason).queue();
    }

    @Override
    public String getHelp() {
        return "Kicks a user from the server. \n"
                + "Usage: " + Constants.PREFIX + getInvoke() + " <User> <Reason> (optional).";
    }

    @Override
    public String getInvoke() {
        return "kick";
    }
}
