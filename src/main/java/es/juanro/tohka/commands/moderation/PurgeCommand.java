package es.juanro.tohka.commands.moderation;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


public class PurgeCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        if (!event.getMember().hasPermission(Permission.MESSAGE_MANAGE)){

            event.getChannel().sendMessage("You need the 'Manage Messages' permisssion to use this command!").queue();

            return;
        }

        if (!event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE)){

            event.getChannel().sendMessage("I need the 'Manage Messages' permisssion to use this command!").queue();

            return;
        }

        if (args.isEmpty()){
            event.getChannel().sendMessage("Not enough arguments! Please check the !t help purge command for more details.").queue();

            return;
        }

        int amount;
        String arg = args.get(0);

        try{
            amount = Integer.parseInt(arg);
        } catch (NumberFormatException ignored){
            event.getChannel().sendMessageFormat("`%s` is not a valid number", arg).queue();

            return;
        }

        if (amount < 2 || amount > 100){
            event.getChannel().sendMessage("The amount must be a number between 2 and 100!").queue();

            return;
        }

        event.getChannel().getIterableHistory()
                .takeAsync(amount)
                .thenApplyAsync((messages) -> {
                    List<Message> goodMessages = messages.stream()
                            .filter(
                                    (m) -> m.getTimeCreated().isBefore(
                                            OffsetDateTime.now().plus(2, ChronoUnit.WEEKS)
                                    ))
                            .collect(Collectors.toList());
                    event.getChannel().purgeMessages(goodMessages);

                    return goodMessages.size();
                })
        .whenCompleteAsync(
                (count, thr) -> event.getChannel().sendMessageFormat("Deleted `%d` messages", count).queue(
                        (message) -> message.delete().queueAfter(10, TimeUnit.SECONDS)
                        )
        )
        .exceptionally((thr) -> {
            String cause = "";

            if (thr.getCause() != null){
                cause = " caused by: " + thr.getCause().getMessage();
            }

            event.getChannel().sendMessageFormat("Error: %s%s", thr.getMessage(), cause).queue();
            return 0;
        });

    }

    @Override
    public String getHelp() {
        return "Clears an specified number of messages from the chat.\n" +
                "Usage: `" + Constants.PREFIX + getInvoke() + "`<amount>";
    }

    @Override
    public String getInvoke() {
        return "purge";
    }
}
