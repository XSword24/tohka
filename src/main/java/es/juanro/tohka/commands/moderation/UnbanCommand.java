package es.juanro.tohka.commands.moderation;

import es.juanro.tohka.Constants;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;
import java.util.stream.Collectors;

public class UnbanCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        if (!event.getMember().hasPermission(Permission.BAN_MEMBERS)) {
            event.getChannel().sendMessage("You need the Ban Members permission to use this command!").queue();
            return;
        }

        if (!event.getGuild().getSelfMember().hasPermission(Permission.BAN_MEMBERS)) {
            event.getChannel().sendMessage("I need the Ban Members permission to use this command!").queue();
            return;
        }

        if (args.isEmpty()) {
            event.getChannel().sendMessage("Not enough arguments! Check the " + Constants.PREFIX + "help command " +
                    "for more details").queue();
        }
        String argsJoined = String.join(" ", args);

        event.getGuild().retrieveBanList().queue((bans) -> {
            List<User> goodUsers = bans.stream().filter((ban) -> isCorrectUser(ban, argsJoined)).map(Guild.Ban::getUser)
                    .collect(Collectors.toList());

            if (goodUsers.isEmpty()) {
                event.getChannel().sendMessage("This user is not banned!").queue();
                return;
            }
            User target = goodUsers.get(0);

            String mod = String.format("%#s", event.getAuthor());
            String bannedUser = String.format("%#s", target);

            event.getGuild().unban(target).reason("Unbanned by" + mod).queue();
            event.getChannel().sendMessage("User " + bannedUser + " has been unbanned.").queue();
        });
    }

    @Override
    public String getHelp() {
        return "Unbans a user from the server. \n" +
                "Usage: " + Constants.PREFIX + getInvoke() + " <Username/Userid/Username#Discriminator>";
    }

    @Override
    public String getInvoke() {
        return "unban";
    }

    private boolean isCorrectUser(Guild.Ban ban, String arg) {
        User bannedUser = ban.getUser();

        return bannedUser.getName().equalsIgnoreCase(arg) || bannedUser.getId().equals(arg) || String.format("%#s", bannedUser).equalsIgnoreCase(arg);
    }
}
