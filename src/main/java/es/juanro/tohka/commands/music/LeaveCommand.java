package es.juanro.tohka.commands.music;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import java.util.List;

public class LeaveCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        AudioManager audioManager = event.getGuild().getAudioManager();

        if (!audioManager.isConnected()) {
            event.getChannel().sendMessage("I have to be in a channel in order to do that!").queue();
            return;
        }

        VoiceChannel voiceChannel = audioManager.getConnectedChannel();

        if (!voiceChannel.getMembers().contains(event.getMember())) {
            event.getChannel().sendMessage("You have to be in the same voice channel as me to use this!").queue();
            return;
        }

        audioManager.closeAudioConnection();
        event.getChannel().sendMessage("Just left the voice channel!").queue();
    }

    @Override
    public String getHelp() {
        return "I leave your voice channel!";
    }

    @Override
    public String getInvoke() {
        return "leave";
    }
}
