package es.juanro.tohka.commands.music;

import es.juanro.tohka.music.GuildMusicManager;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class ResumeCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(event.getGuild());

        if (musicManager.player.getPlayingTrack() == null) {

            event.getChannel().sendMessage("No song is being played right now!").queue();
            return;
        }

        if (!musicManager.player.isPaused()){
            event.getChannel().sendMessage("The song is not paused!").queue();
            return;
        }

        musicManager.player.setPaused(false);
        event.getChannel().sendMessage("Song resumed!").queue();
    }

    @Override
    public String getHelp() {
        return "Resumes a paused song.";
    }

    @Override
    public String getInvoke() {
        return "resume";
    }
}
