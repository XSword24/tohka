package es.juanro.tohka.commands.music;

import es.juanro.tohka.music.GuildMusicManager;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class PauseCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(event.getGuild());

        if (musicManager.player.getPlayingTrack() == null) {

            event.getChannel().sendMessage("No song is being played right now!").queue();
            return;
        }

        if (musicManager.player.isPaused()){
            event.getChannel().sendMessage("The song is already paused!").queue();
            return;
        }

        musicManager.player.setPaused(true);
        event.getChannel().sendMessage("The song has been paused!").queue();
    }

    @Override
    public String getHelp() {
        return "Pauses the current song.";
    }

    @Override
    public String getInvoke() {
        return "pause";
    }
}
