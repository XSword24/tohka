package es.juanro.tohka.commands.music;

import es.juanro.tohka.music.GuildMusicManager;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class StopCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(event.getGuild());

        if (musicManager.player.getPlayingTrack() == null) {

            event.getChannel().sendMessage("No song is being played right now!").queue();
            return;
        }

        musicManager.scheduler.getQueue().clear();
        musicManager.player.stopTrack();
        musicManager.player.setPaused(false);

        event.getChannel().sendMessage("Stopped the song and cleared the queue!").queue();
    }

    @Override
    public String getHelp() {
        return "Stops the music and clears the queue.";
    }

    @Override
    public String getInvoke() {
        return "stop";
    }
}
