package es.juanro.tohka.commands.music;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchResult;
import es.juanro.tohka.Constants;
import es.juanro.tohka.config.Config;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class PlayCommand implements ICommand {
    private final YouTube youTube;

    public PlayCommand(){
        YouTube temp = null;


        try {
            temp = new YouTube.Builder(
                    GoogleNetHttpTransport.newTrustedTransport(),
                    JacksonFactory.getDefaultInstance(),
                    null
            )
                    .setApplicationName("Tohka")
                    .build();
        } catch (Exception e){
            e.printStackTrace();
        }

        youTube = temp;
    }
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        //Check if Tohka is on a channel. If not, make her enter the one the user is in.
        if (!event.getGuild().getAudioManager().isConnected()){

            if (!event.getMember().getVoiceState().inVoiceChannel()) {
                event.getChannel().sendMessage("You have to enter in a voice channel first!").queue();
                return;
            }

            if (!event.getGuild().getSelfMember().hasPermission(Permission.VOICE_CONNECT)) {
                event.getChannel().sendMessage("I don't have the Voice Connect permission!").queue();
                return;
            }

            event.getGuild().getAudioManager().openAudioConnection(event.getMember().getVoiceState().getChannel());
        }

        PlayerManager manager = PlayerManager.getInstance();
        String input = String.join(" ", args);

        if (!isUrl(input)) {
            String ytSearched = searchYoutube(input);

            if (ytSearched == null){
                event.getChannel().sendMessage("No videos found in Youtube!").queue();
                return;
            }

            input = ytSearched;
        }

        manager.loadAndPlay(event.getChannel(), input);

        manager.getGuildMusicManager(event.getGuild()).player.setVolume(10);
    }

    private boolean isUrl(String input) {
        try {
            new URL(input);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    @Nullable
    private String searchYoutube(String input){
        try{
            List<SearchResult> results = youTube.search()
                    .list("id,snippet")
                    .setQ(input)
                    .setMaxResults(1L)
                    .setType("video")
                    .setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)")
                    .setKey(Config.getInstance().getString("youtubekey"))
                    .execute()
                    .getItems();

            if(!results.isEmpty()){
                String videoId = results.get(0).getId().getVideoId();

                return "https://www.youtube.com/watch?v=" + videoId;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String getHelp() {
        return "Plays a song from Youtube/Soundcloud. \n" +
                "Usage: " + Constants.PREFIX + getInvoke() + " <URL/Search parameters (Youtube)>";
    }

    @Override
    public String getInvoke() {
        return "play";
    }
}
