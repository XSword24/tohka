package es.juanro.tohka.commands.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import es.juanro.tohka.music.GuildMusicManager;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NowPlayingCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(event.getGuild());
        AudioPlayer player = musicManager.player;

        if (player.getPlayingTrack() == null) {
            event.getChannel().sendMessage("No song is being played!").queue();

            return;
        }
        AudioTrackInfo info = player.getPlayingTrack().getInfo();
        EmbedBuilder nowPlaying = new EmbedBuilder();
                nowPlaying.setAuthor("Tohka", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png");
                nowPlaying.setColor(new Color(176, 97, 255));
                nowPlaying.setDescription(String.format("**Playing** `%s` \n" + "%s %s - %s", info.title, player.isPaused() ? "\u23F8" : "\u25B6", formatTime(player.getPlayingTrack().getPosition()), formatTime(player.getPlayingTrack().getDuration())));
                event.getChannel().sendMessage(nowPlaying.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Shows the song that is currently being played.";
    }

    @Override
    public String getInvoke() {
        return "nowplaying";
    }

    private String formatTime(long timeInMillis) {
        final long hours = timeInMillis / TimeUnit.HOURS.toMillis(1);
        final long minutes = timeInMillis / TimeUnit.MINUTES.toMillis(1);
        final long seconds = timeInMillis % TimeUnit.MINUTES.toMillis(1) / TimeUnit.SECONDS.toMillis(1);

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
