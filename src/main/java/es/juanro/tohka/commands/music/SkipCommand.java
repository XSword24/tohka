package es.juanro.tohka.commands.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import es.juanro.tohka.music.GuildMusicManager;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.music.TrackScheduler;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class SkipCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(event.getGuild());
        TrackScheduler scheduler = musicManager.scheduler;
        AudioPlayer player = musicManager.player;

        if (player.getPlayingTrack() == null){

            event.getChannel().sendMessage("No song is being played right now!").queue();
            return;
        }

        scheduler.nextTrack();

        event.getChannel().sendMessage("Skipped the current song!").queue();
    }

    @Override
    public String getHelp() {
        return "Skips the current song.";
    }

    @Override
    public String getInvoke() {
        return "skip";
    }
}
