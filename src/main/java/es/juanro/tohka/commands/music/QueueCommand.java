package es.juanro.tohka.commands.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import es.juanro.tohka.music.GuildMusicManager;
import es.juanro.tohka.music.PlayerManager;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class QueueCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        PlayerManager playerManager = PlayerManager.getInstance();
        GuildMusicManager musicManager = playerManager.getGuildMusicManager(event.getGuild());
        BlockingQueue<AudioTrack> queue = musicManager.scheduler.getQueue();

        if (queue.isEmpty()){
            event.getChannel().sendMessage("The queue is empty!").queue();

            return;
        }

        int trackCount = Math.min(queue.size(), 20);
        List<AudioTrack> tracks = new ArrayList<>(queue);
        EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle("Current queue (Total: " + queue.size() + ")");
            builder.setColor(new Color(176, 97, 255));
            builder.setAuthor("Tohka", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png", "https://cdn.discordapp.com/avatars/593445145743982592/ed79bcfda301a06e0516ad5cc94c3738.png");

        for (int i = 0; i < trackCount; i++){
            AudioTrack track = tracks.get(i);
            AudioTrackInfo info = track.getInfo();

            builder.appendDescription(String.format("`%s - %s`\n", info.title, info.author));

        }

        event.getChannel().sendMessage(builder.build()).queue();

    }

    @Override
    public String getHelp() {
        return "Shows the current queue.";
    }

    @Override
    public String getInvoke() {
        return "queue";
    }
}
