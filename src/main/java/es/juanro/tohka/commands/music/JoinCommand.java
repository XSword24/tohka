package es.juanro.tohka.commands.music;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class JoinCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {

        if (event.getGuild().getAudioManager().isConnected()) {
            event.getChannel().sendMessage("I'm already in a channel!").queue();
            return;
        }

        if (!event.getMember().getVoiceState().inVoiceChannel()) {
            event.getChannel().sendMessage("You have to enter in a voice channel first!").queue();
            return;
        }

        if (!event.getGuild().getSelfMember().hasPermission(Permission.VOICE_CONNECT)) {
            event.getChannel().sendMessage("I don't have the Voice Connect permission!").queue();
            return;
        }

        event.getGuild().getAudioManager().openAudioConnection(event.getMember().getVoiceState().getChannel());
        event.getChannel().sendMessage("I'm in your voice channel!").queue();

    }

    @Override
    public String getHelp() {
        return "I join the voice channel you're currently in!";
    }

    @Override
    public String getInvoke() {
        return "join";
    }
}
