package es.juanro.tohka.commands;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LowresCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        ArrayList<File> fileArrayList = new ArrayList<>();
        for (int i = 1; i <= 278; i++) {
            File aux = new File("Images/lowres/" + i + ".jpg");
            fileArrayList.add(aux);
        }
        Random random = new Random();
        event.getChannel().sendFile(fileArrayList.get(random.nextInt(278))).queue();
    }

    @Override
    public String getHelp() {
        return "Sends a random screenshot from the Date A Live anime.";
    }

    @Override
    public String getInvoke() {
        return "lowres";
    }
}
