package es.juanro.tohka.commands;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ServerInfoCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        Guild guild = event.getGuild();

        String generalInfo = String.format(
                "**Server Owner:** <@%s>\n **Region:** %s\n**Verification Level:** %s",
                guild.getOwnerId(),
                guild.getRegion().getName(),
                convertVerificationLevel(guild.getVerificationLevel())
        );

        String memberInfo = String.format(
                "**Total Roles:** %s\n**Total Members:** %s\n",
                guild.getRoleCache().size(),
                guild.getMemberCache().size()
        );

        EmbedBuilder embed = new EmbedBuilder()
                .setColor(new Color(176, 97, 255))
                .setTitle("Server info for: " + guild.getName())
                .setThumbnail(guild.getIconUrl())
                .addField("General Info",generalInfo,false)
                .addField("Role and Member counts",memberInfo, false)
                .addField("Server creation date", event.getGuild().getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME), false)
                ;

        event.getChannel().sendMessage(embed.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Displays information about the server.";
    }

    @Override
    public String getInvoke() {
        return "serverinfo";
    }

    private String convertVerificationLevel(Guild.VerificationLevel level){
        String[] names = level.name().toLowerCase().split("_");
        StringBuilder out = new StringBuilder();

        for (String name : names){
            out.append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).append(" ");
        }

        return out.toString().trim();
    }
}
