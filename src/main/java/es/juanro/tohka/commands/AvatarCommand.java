package es.juanro.tohka.commands;

import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class AvatarCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event) {
        if (event.getMessage().getMentionedUsers().size() == 0) {
            event.getChannel().sendMessage(event.getAuthor().getAvatarUrl()).queue();
        }
        if (event.getMessage().getMentionedUsers().size() != 0) {
            User user = event.getMessage().getMentionedUsers().get(0);
            event.getChannel().sendMessage(user.getAvatarUrl()).queue();
        }
    }

    @Override
    public String getHelp() {
        return "Displays the avatar of the user who has used the command. With !avatar <Mention> you can see the avatar of other users as well.";
    }

    @Override
    public String getInvoke() {
        return "avatar";
    }
}
