package es.juanro.tohka;

import es.juanro.tohka.commands.*;
import es.juanro.tohka.commands.admin.SetPrefixCommand;
import es.juanro.tohka.commands.convert.*;
import es.juanro.tohka.commands.moderation.BanCommand;
import es.juanro.tohka.commands.moderation.KickCommand;
import es.juanro.tohka.commands.moderation.PurgeCommand;
import es.juanro.tohka.commands.moderation.UnbanCommand;
import es.juanro.tohka.commands.music.*;
import es.juanro.tohka.commands.music.PlayCommand;
import es.juanro.tohka.objects.ICommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.regex.Pattern;

public class CommandManager {

    private final Map<String, ICommand> commands = new HashMap<>();

    CommandManager() {
        addCommand(new PingCommand());
        addCommand(new HelpCommand(this));
        addCommand(new AvatarCommand());
        addCommand(new LowresCommand());
        addCommand(new UwuCommand());
        addCommand(new UserInfoCommand());
        addCommand(new KickCommand());
        addCommand(new BanCommand());
        addCommand(new UnbanCommand());
        addCommand(new SetPrefixCommand());
        addCommand(new JoinCommand());
        addCommand(new LeaveCommand());
        addCommand(new PlayCommand());
        addCommand(new StopCommand());
        addCommand(new QueueCommand());
        addCommand(new SkipCommand());
        addCommand(new NowPlayingCommand());
        addCommand(new PauseCommand());
        addCommand(new ResumeCommand());
        addCommand(new C2FCommand());
        addCommand(new F2CCommand());
        addCommand(new K2MCommand());
        addCommand(new M2KCommand());
        addCommand(new Ft2CmCommand());
        addCommand(new Cm2FtCommand());
        addCommand(new ServerInfoCommand());
        addCommand(new PurgeCommand());
    }

    public Collection<ICommand> getCommands() {
        return commands.values();
    }

    public ICommand getCommand(@NotNull String name) {
        return commands.get(name);
    }

    private void addCommand(ICommand command) {
        if (!commands.containsKey(command.getInvoke())) {
            commands.put(command.getInvoke(), command);
        }
    }

    void handleCommand(GuildMessageReceivedEvent event) {
        if (event.getMessage().getContentRaw().startsWith(Constants.PREFIX)) {
            final String prefix = Constants.PREFIX;
            final String[] split = event.getMessage().getContentRaw().replaceFirst(
                    "(?i)" + Pattern.quote(prefix), "").split("\\s+");
            final String invoke = split[0].toLowerCase();

            if (commands.containsKey(invoke)) {
                final List<String> args = Arrays.asList(split).subList(1, split.length);

                commands.get(invoke).handle(args, event);
            }
            if (!commands.containsKey(invoke)) {
                event.getChannel().sendMessage("That command does not exist! Please use !t help for a list of commands.").queue();
            }
        } else {
            final String prefix = Constants.PREFIXES.get(event.getGuild().getIdLong());
            final String[] split = event.getMessage().getContentRaw().replaceFirst(
                    "(?i)" + Pattern.quote(prefix), "").split("\\s+");
            final String invoke = split[0].toLowerCase();

            if (commands.containsKey(invoke)) {
                final List<String> args = Arrays.asList(split).subList(1, split.length);

                commands.get(invoke).handle(args, event);
            }
            if (!commands.containsKey(invoke)) {
                event.getChannel().sendMessage("That command does not exist! Please use !t help for a list of commands.").queue();
            }
        }
    }
}
