package es.juanro.tohka;

import es.juanro.tohka.config.Config;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;

public class Main {

    private Main() throws IOException {
        Config config = new Config(new File("botconfig.json"));
        CommandManager commandManager = new CommandManager();
        Listener listener = new Listener(commandManager);
        try {
            new JDABuilder(AccountType.BOT)
                    .setActivity(Activity.playing("!t help"))
                    .setToken(config.getString("token"))
                    .addEventListeners(listener)
                    .build().awaitReady();
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        new Main();

    }
}
