package es.juanro.tohka;

import es.juanro.tohka.config.Config;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

class Listener extends ListenerAdapter {

    private final CommandManager manager;

    Listener (CommandManager manager){
        this.manager = manager;
    }
    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        //shutdown method
        if (event.getMessage().getContentRaw().equalsIgnoreCase(Constants.PREFIX + "shutdown") &&
                event.getAuthor().getIdLong() == Long.parseLong(Config.getInstance().getString("owner"))){

            shutdown(event.getJDA());
            return;

        }

        String prefix = Constants.PREFIXES.computeIfAbsent(event.getGuild().getIdLong(), (l) -> Constants.PREFIX);
        //Ignore bots and webhooks. Also make sure that Tohka only reacts if the prefix is written.

        if(!event.getAuthor().isBot() && !event.getMessage().isWebhookMessage() &&
                (event.getMessage().getContentRaw().startsWith(prefix))|| event.getMessage().getContentRaw().startsWith(Constants.PREFIX)) {

            manager.handleCommand(event);

        }
    }

    private void shutdown (JDA jda){

        jda.shutdown();
    }
}
